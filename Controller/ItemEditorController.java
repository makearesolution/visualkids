package sample.Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ItemEditorController {
    @FXML
    private TextField txtUnitPrice;

    @FXML
    private Button btnPicture;

    @FXML
    private TextField txtPicturePath;

    @FXML
    private TextField txtItemNumber;

    @FXML
    private TextField txtItemName;

    @FXML
    private Button btnNewCategory;

    @FXML
    private Button btnNewItemType;

    @FXML
    private ComboBox<?> cbxCategories;

    @FXML
    private ComboBox<?> cbxItemTypes;

    @FXML
    private ImageView pbxPicturePath;

    @FXML
    private Button btnCreate;

    @FXML
    private Button btnClose;

    public void btnNewCategory_Click(ActionEvent event){
        try {
            GridPane root = FXMLLoader.load(getClass().getResource("ItemCategory.fxml"));
            Scene scene = new Scene(root,300,100);
            Stage stage = new Stage();
            stage.setTitle("Item Category");
            stage.setScene(scene);
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
