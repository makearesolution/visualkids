package sample.Controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import sample.Model.ItemCategory;
import sample.Model.ItemCategoryDAO;

import java.sql.SQLException;

public class OrderProcessingController {
    @FXML
    private TextField txtItemsTotal;

    @FXML
    private TextField txtRate;

    @FXML
    private TextField txtTaxAmount;

    @FXML
    private TextField txtOrderTotal;

    @FXML
    private Button btnSave;

    @FXML
    private TextField txtReceiptNumber;

    @FXML
    private Button btnOpen;

    @FXML
    private Button btnNewOrder;

    @FXML
    private Button btnClose;

    @FXML
    private ImageView pbxStoreItem;

    @FXML
    private ComboBox<String> cbxCategories;

    @FXML
    private ComboBox<String> cbxTypes;

    @FXML
    private Button btnNewStoreItem;

    @FXML
    private TableView<ItemCategory> lvwStoreItems;

    @FXML
    private TableColumn<ItemCategory, Integer> colItemNumber;

    @FXML
    private TableColumn<ItemCategory, String> colItemName;

    @FXML
    private TableColumn<ItemCategory, Integer> colUnitPrice;

    @FXML
    void btnClose_Click(ActionEvent event) {

    }

    @FXML
    private void initialize(){
        colItemNumber.setCellValueFactory(e -> e.getValue().itemIdProperty().asObject());
        colItemName.setCellValueFactory(e -> e.getValue().itemStringProperty());
        colUnitPrice.setCellValueFactory(e -> e.getValue().itemUnitPriceProperty().asObject());
    }
    @FXML
    private void populateAndShowItemCategory(ItemCategory itemCategory) throws ClassNotFoundException{
        ObservableList<ItemCategory> itemCategoryObservableList = FXCollections.observableArrayList();
        itemCategoryObservableList.add(itemCategory);
        lvwStoreItems.setItems(itemCategoryObservableList);
    }
    @FXML
    void btnNewCustomerOrder_Click(ActionEvent event) {

    }

    @FXML
    void btnNewStoreItem_Click(ActionEvent event) {

    }

    @FXML
    void btnOpen_Click(ActionEvent event) throws SQLException, ClassNotFoundException {
        try {
            ItemCategory itemCategory = ItemCategoryDAO.seachItemCategory(txtReceiptNumber.getText());
            populateAndShowItemCategory(itemCategory);
        }catch(SQLException e){
            e.printStackTrace();
            colItemName.setText("Error occurred while getting employee information from DB." + e);
            throw e;
        }
    }

    @FXML
    void btnSave_Click(ActionEvent event) {

    }
}
