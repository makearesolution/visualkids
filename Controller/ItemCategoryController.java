package sample.Controller;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ItemCategoryController {
    @FXML
    public TextField txtCategory;

    @FXML
    private Button btnOK;

    @FXML
    private Button btnCancel;

    @FXML
    void Cancel(ActionEvent event) {
        System.out.println("canceled");
    }

    @FXML
    void OK(ActionEvent event) {
        System.out.println("OK");
    }
}
