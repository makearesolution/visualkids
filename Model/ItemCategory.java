package sample.Model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ItemCategory {
    private IntegerProperty ItemId;
    private StringProperty ItemCategory;
    private StringProperty ItemType;
    private StringProperty ItemName;
    private IntegerProperty ItemUnitPrice;
    private StringProperty ItemPicture;

    public ItemCategory(){
        this.ItemId = new SimpleIntegerProperty();
        this.ItemCategory = new SimpleStringProperty();
        this.ItemType = new SimpleStringProperty();
        this.ItemName = new SimpleStringProperty();
        this.ItemUnitPrice = new SimpleIntegerProperty();
        this.ItemPicture = new SimpleStringProperty();
    }

    public int getItemId() {
        return ItemId.get();
    }

    public IntegerProperty itemIdProperty() {
        return ItemId;
    }

    public void setItemId(int itemId) {
        this.ItemId.set(itemId);
    }

    public String getItemCategory() {
        return ItemCategory.get();
    }

    public StringProperty itemCategoryProperty() {
        return ItemCategory;
    }

    public void setItemCategory(String itemCategory) {
        this.ItemCategory.set(itemCategory);
    }

    public String getItemType() {
        return ItemType.get();
    }

    public StringProperty itemTypeProperty() {
        return ItemType;
    }

    public void setItemType(String itemType) {
        this.ItemType.set(itemType);
    }

    public String getItemString() {
        return ItemName.get();
    }

    public StringProperty itemStringProperty() {
        return ItemName;
    }

    public void setItemName(String itemString) {
        this.ItemName.set(itemString);
    }

    public int getItemUnitPrice() {
        return ItemUnitPrice.get();
    }

    public IntegerProperty itemUnitPriceProperty() {
        return ItemUnitPrice;
    }

    public void setItemUnitPrice(int itemUnitPrice) {
        this.ItemUnitPrice.set(itemUnitPrice);
    }

    public String getItemPicture() {
        return ItemPicture.get();
    }

    public StringProperty itemPictureProperty() {
        return ItemPicture;
    }

    public void setItemPicture(String itemPicture) {
        this.ItemPicture.set(itemPicture);
    }
}
