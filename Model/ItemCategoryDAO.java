package sample.Model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.Util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemCategoryDAO {
    public static ItemCategory seachItemCategory(String ItemId) throws SQLException,ClassNotFoundException {
        String selectStmt =  "SELECT * FROM ItemCategories WHERE employee_id=" + ItemId;
        try {
            ResultSet rsEmp = DBUtil.dbExecuteQuery(selectStmt);
            ItemCategory itemCategory = getItemCategoryFromResultSet(rsEmp);
            return itemCategory;
        }catch (Exception e){
            System.out.println("While searching an employee with " + ItemId + " id, an error occurred: " +
                    e);
            throw e;
        }
    }
    public static ItemCategory getItemCategoryFromResultSet(ResultSet resultSet) throws SQLException {
        ItemCategory itemCategory = null;
        if(resultSet.next()){
            itemCategory = new ItemCategory();
            itemCategory.setItemId(resultSet.getInt("ItemId"));
            itemCategory.setItemCategory(resultSet.getString("ItemCategory"));
            itemCategory.setItemPicture(resultSet.getString("ItemPicture"));
            itemCategory.setItemName(resultSet.getString("ItemName"));
            itemCategory.setItemType(resultSet.getString("ItemType"));
            itemCategory.setItemUnitPrice(resultSet.getInt("ItemUnitPrice"));
        }
        return itemCategory;
    }

    public static ObservableList<ItemCategory> searchItemCategories() throws SQLException,ClassNotFoundException{
        String selectStmt = "SELECT * FROM ItemCategories";
        try {
            ResultSet rsEmps = DBUtil.dbExecuteQuery(selectStmt);
            ObservableList<ItemCategory> itemCategoryObservableList = getItemCategoryList(rsEmps);
            return itemCategoryObservableList;
        }catch (Exception e){
            System.out.println("SQL select operation has been failed: " + e);
            //Return exception
            throw e;
        }
    }
    private static ObservableList<ItemCategory> getItemCategoryList(ResultSet resultSet) throws SQLException,ClassNotFoundException{
        ObservableList<ItemCategory> itemCategoryObservableList = FXCollections.observableArrayList();

        while (resultSet.next()){
            ItemCategory itemCategoryList = new ItemCategory();
            itemCategoryList.setItemId(resultSet.getInt("ItemId"));
            itemCategoryList.setItemCategory(resultSet.getString("ItemCategory"));
            itemCategoryList.setItemPicture(resultSet.getString("ItemPicture"));
            itemCategoryList.setItemName(resultSet.getString("ItemName"));
            itemCategoryList.setItemType(resultSet.getString("ItemType"));
            itemCategoryList.setItemUnitPrice(resultSet.getInt("ItemUnitPrice"));
            itemCategoryObservableList.add(itemCategoryList);
        }
        return itemCategoryObservableList;
    }

    public static void insertItemCategory(Integer ItemId, String ItemCategory, String ItemPicture, String ItemName, String ItemType, Integer ItemUnitPrice) throws SQLException, ClassNotFoundException{
        String updateStmt =
                "BEGIN\n" +
                        "INSERT INTO employees" +
                        "(ItemId, ItemCategory, ItemType, ItemName, ItemUnitPrice, ItemPicture)" +
                        "VALUES" +
                        "(sequence_employee.nextval, '"+ItemId+"', '"+ItemCategory+"','"+ItemPicture+"', '"+ItemName+"', '"+ItemType+"', '"+ItemUnitPrice+"');" +
        "END;";
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }

    }
}
